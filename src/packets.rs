use serde_mcpacket::{packet, varlen::VarInt};

use crate::State;

packet!(Handshake, 0, {
	ver: VarInt,
	server_addr: String,
	port: u16,
	next_state: State
});

packet!(SetCompression, 3, { threshold: VarInt });

packet!(StatusResponse, 0, { response: String });

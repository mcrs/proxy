mod packets;

use std::{
	collections::HashMap,
	io::{Cursor, ErrorKind, Read, Write},
	net::{SocketAddr, TcpListener, TcpStream},
	sync::mpsc::channel,
	time::Duration,
};

use argh::FromArgs;
use serde::{Deserialize, Serialize};
use serde_mcpacket::{
	packet::RawPacket,
	varlen::{VarInt, VarLen},
};
use time::OffsetDateTime;
use tracing::Level;
use tracing_log::LogTracer;
use tracing_subscriber::FmtSubscriber;
use uuid::Uuid;

/// Minecraft 1.8.9 proxy
#[derive(Debug, FromArgs)]
pub struct Args {
	/// address to listen on
	#[argh(positional)]
	bind_addr: SocketAddr,
	/// server address to proxy to
	#[argh(positional)]
	dest_server_addr: SocketAddr,
}

use crate::packets::{Handshake, SetCompression, StatusResponse};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Deserialize, Serialize)]
pub enum State {
	Handshaking = 0,
	Status = 1,
	Login = 2,
	Play = 3,
}

pub struct PacketBuffer {
	expected_amount: usize,
	offset: usize,
	buffer: Vec<u8>,
}

impl AsMut<[u8]> for PacketBuffer {
	fn as_mut(&mut self) -> &mut [u8] {
		&mut self.buffer[self.offset..self.expected_amount]
	}
}

impl AsRef<[u8]> for PacketBuffer {
	fn as_ref(&self) -> &[u8] {
		self.buffer.as_slice()
	}
}

impl PacketBuffer {
	pub fn increase(&mut self, amount: usize) -> bool {
		self.offset += amount;
		self.offset >= self.expected_amount
	}

	pub fn take(self) -> Vec<u8> {
		self.buffer
	}

	pub fn read(expected_amount: usize) -> Self {
		Self { expected_amount, offset: 0, buffer: vec![0u8; expected_amount] }
	}

	pub fn write(buffer: Vec<u8>) -> Self {
		Self { expected_amount: buffer.len(), offset: 0, buffer }
	}
}

pub struct Connection {
	pub id: Uuid,
	pub addr: SocketAddr,
	pub compression: Option<i32>,
	pub state: State,
	pub client_stream: TcpStream,
	pub client_read_buffer: Option<PacketBuffer>,
	pub client_write_buffer: Option<PacketBuffer>,
	pub server_stream: TcpStream,
	pub server_read_buffer: Option<PacketBuffer>,
	pub server_write_buffer: Option<PacketBuffer>,
	pub last_successful: OffsetDateTime,
	pub closed: bool,
}

impl Connection {
	pub fn new(
		id: Uuid,
		addr: SocketAddr,
		client_stream: TcpStream,
		server_stream: TcpStream,
	) -> Self {
		Self {
			id,
			addr,
			compression: None,
			state: State::Handshaking,
			client_stream,
			server_stream,
			client_read_buffer: None,
			server_read_buffer: None,
			last_successful: OffsetDateTime::now_utc(),
			closed: false,
			client_write_buffer: None,
			server_write_buffer: None,
		}
	}

	fn poll_stream(
		read_buffer: &mut Option<PacketBuffer>,
		stream: &mut TcpStream,
		compression_threshold: Option<i32>,
	) -> Result<Option<RawPacket>, std::io::Error> {
		let res = match read_buffer {
			Some(packet_buffer) => match stream.read(packet_buffer.as_mut()) {
				Ok(x) => {
					if packet_buffer.increase(x) {
						let packet = RawPacket::from_read(
							&mut Cursor::new(packet_buffer.as_ref()),
							compression_threshold,
						)
						.unwrap();
						Ok(Some(packet))
					} else {
						Ok(None)
					}
				}
				Err(e) if e.kind() == ErrorKind::WouldBlock => Ok(None),
				Err(err) => Err(err),
			},
			None => {
				let mut varint_buffer = [0u8; 5];
				match stream.peek(&mut varint_buffer) {
					Ok(x) => {
						let mut found = false;
						if x == 5 {
							found = true;
						} else {
							for y in varint_buffer.iter().take(x) {
								if *y & (1 << 7) == 0 {
									found = true;
									break;
								}
							}
						}

						if !found {
							return Ok(None);
						}
					}
					Err(e) if e.kind() == ErrorKind::WouldBlock => {
						return Ok(None);
					}
					Err(err) => return Err(err),
				};

				let length = VarInt::from_bytes(&varint_buffer);
				if length.0.is_negative() {
					panic!("negative packet length");
				}

				if length.0 > 1000000 {
					panic!("packet over 1mb ({}, {:#?})", length.0, varint_buffer);
				}

				tracing::info!("packet length {}", length.0);

				*read_buffer =
					Some(PacketBuffer::read(length.0 as usize + length.count_bytes() as usize));
				Self::poll_stream(read_buffer, stream, compression_threshold)
			}
		};

		if res.as_ref().map(|x| x.is_some()).unwrap_or_default() {
			*read_buffer = None;
		}

		res
	}

	fn handle_io_error<T>(err: Result<T, std::io::Error>) -> Result<Option<T>, std::io::Error> {
		match err {
			Ok(res) => Ok(Some(res)),
			Err(why) if why.kind() == ErrorKind::WouldBlock => Ok(None),
			Err(why) => Err(why),
		}
	}

	pub fn poll_server(&mut self) -> Result<Option<RawPacket>, std::io::Error> {
		Self::poll_stream(&mut self.server_read_buffer, &mut self.server_stream, self.compression)
	}

	pub fn poll_client(&mut self) -> Result<Option<RawPacket>, std::io::Error> {
		Self::poll_stream(&mut self.client_read_buffer, &mut self.client_stream, self.compression)
	}

	pub fn poll(&mut self) -> Result<bool, std::io::Error> {
		if self.closed {
			return Ok(false);
		}

		let span = tracing::span!(
			Level::INFO,
			"poll",
			"state: {:#?}, id: {}",
			self.state,
			self.id.to_string()
		);
		let _guard = span.enter();

		// Serverbound
		let take_buffer = match self.server_write_buffer.as_mut() {
			Some(buffer) => {
				let res = Self::handle_io_error(self.server_stream.write(buffer.as_mut()))?
					.unwrap_or_default();
				buffer.increase(res)
			}
			None => {
				if let Some(raw_packet) = self.poll_client()? {
					let id = raw_packet.packet_id;
					tracing::info!("Read serverbound packet {:#X}", id);

					let packet_data = match self.compression {
						Some(threshold) => raw_packet.finalize_compressed(threshold),
						_ => raw_packet.finalize_decompressed(),
					}
					.unwrap();

					let mut buffer = PacketBuffer::write(packet_data);
					let res = self.server_stream.write(buffer.as_mut())?;
					if !buffer.increase(res) {
						self.server_write_buffer = Some(buffer);
					}

					self.last_successful = OffsetDateTime::now_utc();
					tracing::info!("Written (and now operating on) serverbound packet {:#X}", id);

					match self.state {
						State::Handshaking => {
							if id == 0 {
								if let Ok(handshake) = raw_packet.deserialize::<Handshake>() {
									tracing::info!(
										"Connection is entering {:?}",
										handshake.next_state
									);
									self.state = handshake.next_state;
								}
							}
						}
						State::Status | State::Login | State::Play => {}
					}
				};
				false
			}
		};

		if take_buffer {
			self.server_write_buffer = None;
		}

		// Clientbound
		let take_buffer = match self.client_write_buffer.as_mut() {
			Some(buffer) => {
				let res = Self::handle_io_error(self.client_stream.write(buffer.as_mut()))?
					.unwrap_or_default();
				buffer.increase(res)
			}
			None => {
				if let Some(raw_packet) = self.poll_server()? {
					let id = raw_packet.packet_id;
					tracing::info!("Read clientbound packet {:#X}", id);

					let packet_data = match self.compression {
						Some(threshold) => raw_packet.finalize_compressed(threshold),
						_ => raw_packet.finalize_decompressed(),
					}
					.unwrap();

					let mut buffer = PacketBuffer::write(packet_data);
					let res = self.client_stream.write(buffer.as_mut())?;
					if !buffer.increase(res) {
						self.client_write_buffer = Some(buffer);
					}

					self.last_successful = OffsetDateTime::now_utc();
					tracing::info!("Written (and now operating on) clientbound packet {:#X}", id);

					if self.state == State::Login {
						match id {
							2 => {
								tracing::info!("Connection entered play");
								self.state = State::Play;
							}
							3 => {
								if let Ok(set_compression) =
									raw_packet.deserialize::<SetCompression>()
								{
									let threshold = set_compression.threshold.0;
									tracing::info!(
										"Connection set_compression with threshold: {}",
										threshold
									);
									if threshold.is_negative() {
										self.compression = None;
									} else {
										self.compression = Some(threshold);
									}
								}
							}
							_ => {}
						}
					} else if self.state == State::Status {
						match id {
							0 => match raw_packet.deserialize::<StatusResponse>() {
								Ok(packet) => {
									tracing::info!("Json: {}", packet.response);
								}
								Err(why) => {
									tracing::error!("Deser error: {:?}", why);
								}
							},
							_ => {}
						}
					}
				};

				false
			}
		};

		if take_buffer {
			self.client_write_buffer = None;
		}

		if OffsetDateTime::now_utc() > self.last_successful + Duration::from_secs(10) {
			tracing::info!("Timeouting...");
			self.closed = true;
			return Ok(false);
		}

		Ok(true)
	}
}

fn main() {
	let subscriber = FmtSubscriber::builder().with_max_level(tracing::Level::TRACE).finish();
	tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");
	LogTracer::init().unwrap();

	let args: Args = argh::from_env();

	let (tx, rx) = channel();

	let tcp_listener_jh = std::thread::spawn(move || {
		let conn = TcpListener::bind(args.bind_addr).unwrap();
		loop {
			match conn.accept() {
				Ok(x) => {
					if tx.send(x).is_err() {
						tracing::error!("TX is dead... closing server");
						break;
					}
				}
				Err(why) => {
					tracing::error!("Failed to accept TCP connection: {:?}", why);
				}
			}
		}
	});

	let mut connections: HashMap<Uuid, Connection> = HashMap::new();
	'proxy: loop {
		// Only accept up to 64 connections per iteration
		'accept_new_connections: for _ in 0..64 {
			match rx.try_recv() {
				Ok((stream, socketaddr)) => {
					if stream.set_nonblocking(true).is_ok() {
						if let Ok(server_stream) = TcpStream::connect(args.dest_server_addr) {
							if server_stream.set_nonblocking(true).is_ok() {
								let uuid = Uuid::new_v4();
								tracing::info!("Accepted connection {} from {}", uuid, socketaddr);
								connections.insert(
									uuid,
									Connection::new(uuid, socketaddr, stream, server_stream),
								);
							}
						}
					}
				}
				Err(std::sync::mpsc::TryRecvError::Empty) => break 'accept_new_connections,
				Err(_) => {
					tracing::info!("TX died");
					break 'proxy;
				}
			}
		}

		// We want to iterate over the hashmaps contents in it's current state so we
		// copy the UUIDs out first
		let keys = connections.keys().copied().collect::<Vec<_>>();
		for key in &keys {
			let mut close_connection = false;

			if let Some(connection) = connections.get_mut(key) {
				match connection.poll() {
					// Connection timed out
					Ok(false) => {
						close_connection = true;
					}
					// Everything is ok
					Ok(true) => {}
					// Unexpected IO error
					Err(why) => {
						tracing::error!("Unexpected IO error: {:?} {}", why.kind(), why);
						close_connection = true;
					}
				}
			}

			// If connection is broken for some reason remove it from the map
			if close_connection {
				tracing::info!("Closing connection for {}", key);
				connections.remove(key);
			}
		}
	}

	tracing::info!("Waiting for JH...");
	tcp_listener_jh.join().unwrap();
}
